import java.util.regex.Pattern;

public class Pointer {
    private String name;
    private double x;
    private double y;
    public Pointer(String name, int x, int y){
        this.name=name;
        this.x=x;
        this.y=y;
    }

    public String getName() {
        return name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    public double getDistance(){
        double distance = Math.sqrt((Math.pow(x,2)+Math.pow(y,2)));
        return distance;
    }

    @Override
    public String toString() {
        return "Point{"+
                "point = " + getName() +
                "X = "+getX()+
                "Y = "+getY()+
                "}";
    }

}
