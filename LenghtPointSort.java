
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class LenghtPointSort {
    static ArrayList<Pointer> point = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        listPoint();
        pointSorter();
        printPoint();

    }
    public static void listPoint(){
        System.out.println("Porównywanie odległości punktów od środka układu");
        System.out.println("Podaj ile punktów chcesz zbadać");
        int numberOfPoint = scanner.nextInt();
        System.out.println("Podaj nazwe i wspułrzędne punktów oddzielając je spacjami");
        for (int i = 0; i <numberOfPoint ; i++) {
            System.out.println("podaj " + (i+1));
            String name = scanner.next();
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Pointer dath = new Pointer(name,x,y);
            point.add(dath);
        }
    }
    public static void pointSorter(){
        Comparator<Pointer> pointComper = (p1 ,p2) ->(int) (p1.getDistance()-p2.getDistance());
        point.sort(pointComper);
    }
    public static void printPoint(){
        for (Pointer point : point){
            System.out.println(point);
        }
    }
}
